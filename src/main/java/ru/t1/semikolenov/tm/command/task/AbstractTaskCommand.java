package ru.t1.semikolenov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.api.service.IProjectTaskService;
import ru.t1.semikolenov.tm.api.service.ITaskService;
import ru.t1.semikolenov.tm.command.AbstractCommand;
import ru.t1.semikolenov.tm.enumerated.Role;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    public ITaskService getTaskService() {
        return serviceLocator.getTaskService();
    }

    @NotNull
    public IProjectTaskService getProjectTaskService() {
        return serviceLocator.getProjectTaskService();
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
