package ru.t1.semikolenov.tm.exception.user;

import ru.t1.semikolenov.tm.exception.AbstractException;

public final class AuthenticationException extends AbstractException {

    public AuthenticationException() {
        super("Error! Authentication failed...");
    }

}
