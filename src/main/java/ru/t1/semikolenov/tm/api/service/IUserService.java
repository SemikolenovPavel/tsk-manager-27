package ru.t1.semikolenov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.enumerated.Role;
import ru.t1.semikolenov.tm.model.User;

import java.util.List;

public interface IUserService extends IService<User>{

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    boolean isLoginExists(@Nullable String login);

    boolean isEmailExists(@Nullable String email);

    @NotNull
    User removeByLogin(@NotNull String login);

    @NotNull
    User create(@NotNull String login, @NotNull String password);

    @NotNull
    User create(
            @NotNull String login,
            @NotNull String password,
            @Nullable String email
    );

    @NotNull
    User create(
            @NotNull String login,
            @NotNull String password,
            @Nullable Role role
    );

    @NotNull
    User setPassword(@NotNull String userId, @NotNull String password);

    @NotNull
    User updateUser(
            @NotNull String userId,
            @Nullable String firstName,
            @Nullable String lastName,
            @Nullable String middleName
    );

    void lockUserByLogin(@NotNull String login);

    void unlockUserByLogin(@NotNull String login);

}
