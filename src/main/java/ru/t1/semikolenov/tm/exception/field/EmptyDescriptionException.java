package ru.t1.semikolenov.tm.exception.field;

public final class EmptyDescriptionException extends AbstractFieldException {

    public EmptyDescriptionException() {
        super("Error! Description is empty...");
    }

}
