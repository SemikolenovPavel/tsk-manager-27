package ru.t1.semikolenov.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.security.NoSuchAlgorithmException;

public interface HashUtil {

    @Nullable
    static String salt(
            @Nullable final String value,
            @Nullable final String secret,
            @Nullable final Integer iteration
    ) {
        if (value == null || secret == null || iteration == null) return null;
        @NotNull String result = value;
        for (int i = 0; i < iteration; i++) {
            result = md5(secret + result + secret);
        }
        return result;
    }

    @NotNull
    static String md5(@NotNull String value) {
        try {
            @NotNull java.security.MessageDigest md =
                    java.security.MessageDigest.getInstance("MD5");
            @NotNull byte[] array = md.digest(value.getBytes());
            @NotNull StringBuffer sb = new StringBuffer();
            for (int i = 0; i < array.length; i++) {
                sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100))
                        .substring(1, 3);
            }
            return sb.toString();
        } catch (@NotNull NoSuchAlgorithmException e) {
            throw new RuntimeException();
        }
    }

}
